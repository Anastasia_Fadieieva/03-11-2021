﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectBD_03._11
{
    internal class PersonModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string City { get; set; }
        public int Price { get; set; }
    }
}
