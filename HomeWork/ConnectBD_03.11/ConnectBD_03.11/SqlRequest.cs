﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.SqlServer;

namespace ConnectBD_03._11
{   
    static class SqlRequests
    {
        private static readonly string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=hw_db;Integrated Security=True";
        private static SqlConnection sqlConnection = null;
        static SqlCommand sqlCommand = null;

        public static List<PersonModel> Execute(string command)
        {
            PersonModel person = new PersonModel();
            List<PersonModel> people = new List<PersonModel>();
            sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            //SqlDataReader sqlDataReader = null;
            sqlCommand = new SqlCommand(command, sqlConnection);
            switch (command.Split(' ')[0].ToLower())
            {
                case "select":
                    people = Select(sqlCommand);
                    break;
                case "insert":
                    SqlDataReader sqlDataReader = null;
                    sqlDataReader = sqlCommand.ExecuteReader();
                    break;
            }
            return people;
        }
        public static List<PersonModel> Select(SqlCommand sqlCommand)
        {
            List<PersonModel> people = new List<PersonModel>();
            SqlDataReader sqlDataReader = null;
            sqlDataReader = sqlCommand.ExecuteReader();
            while (sqlDataReader.Read())
            {
                PersonModel person = new PersonModel();
                //person.Id = (int)sqlDataReader["Id"]; 
                person.FirstName = (string)sqlDataReader["first_name"];
                //person.LastName = (string)sqlDataReader["LastName"]; 
                person.Price = (int)sqlDataReader["price"];
                //person.City = (string)sqlDataReader["City"];
                people.Add(person);
            }
            if (sqlDataReader != null)
            {
                sqlDataReader.Close();
            }
            return people;
        }

    }
}
