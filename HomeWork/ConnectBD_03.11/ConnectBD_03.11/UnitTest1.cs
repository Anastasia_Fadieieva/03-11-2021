using NUnit.Framework;
using System.Collections.Generic;

namespace ConnectBD_03._11
{
    public class Tests
    {
       
        [TestCase("first_name", "Alina")]
        [TestCase("first_name", "Nastya")]
        [TestCase("price", "321")]
        public void CheckSelectCommands(string parametr, object stringvalue)
        {
            string command = $"Select * From [Orders] Where {parametr}='{stringvalue}'";
            List<PersonModel> people = SqlRequests.Execute(command);
            foreach (var person in people)
            {
                switch (parametr)
                {
                    case "first_name":
                        Assert.AreEqual(person.FirstName, (string)stringvalue);
                        break;
                    case "price":
                        Assert.AreEqual(person.Price.ToString(), stringvalue);
                        break;
                }
            }
        }
        [TestCase("first_name", "Tysya")]
        [TestCase("price", null)]
        public void CheckDeleteCommands(string parametr, object stringvalue)
        {
            string command = $"DELETE FROM [dbo].[Orders] WHERE {parametr}='{stringvalue}';";
            List<PersonModel> people = SqlRequests.Execute(command);
            foreach (var person in people)
            {
                switch (parametr)
                {
                    case "first_name":
                        Assert.AreEqual(person.FirstName, (string)stringvalue);
                        break;
                    case "price":
                        Assert.AreEqual(person.Price.ToString(), stringvalue);
                        break;
                }
            }
        }
        [TestCase("first_name", "Taras", "Mihail")]
        [TestCase("price", null, 123)]
        public void CheckUpdateCommands(string parametr, object stringValue, object stringValue2)
        {
            string command = "UPDATE [dbo].[Orders]" +
                              $"SET {parametr} = '{stringValue2}'"+
                              $"WHERE {parametr}='{stringValue}';";
            
            List<PersonModel> people = SqlRequests.Execute(command);
            foreach (var person in people)
            {
                switch (parametr)
                {
                    case "first_name":
                        Assert.AreEqual(person.FirstName, (string)stringValue2);
                        break;
                    case "price":
                        Assert.AreEqual(person.Price.ToString(), stringValue2);
                        break;
                }
            }
        }

    }
}